import { TrackingService } from 'src/services/trackingService';

export class BaseHomeComponent {
  
  items: number[] = [];

  constructor(trackingService: TrackingService) {
    for(var i = 1; i <= 10; i++) {
      this.items.push(i);
    }

    trackingService.trackPageView("/home");
  }
}
