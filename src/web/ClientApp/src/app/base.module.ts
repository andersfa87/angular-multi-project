import { NgModule } from '@angular/core';
import { TrackingService } from 'src/services/trackingService';

@NgModule({
    declarations: [],
    imports: [ ],
    exports: [],
    providers: [TrackingService],
})
export class BaseModule {

}