import { Component, OnInit } from '@angular/core';
import { TrackingService } from 'src/services/trackingService';

export class BaseVipComponent implements OnInit {
    constructor(trackingService: TrackingService) {
        trackingService.trackPageView("/vip");
    }

    ngOnInit(): void { 
        console.log("onInit")
    }
}
