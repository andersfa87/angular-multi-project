import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mobile-app';

  constructor() {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
  }

  

  // deviceready Event Handler
  //
  // Bind any cordova events here. Common events are:
  // 'pause', 'resume', etc.
  onDeviceReady() {
      this.receivedEvent('deviceready');
  }

  // Update DOM on a Received Event
  receivedEvent(id) {
      
      console.log('Received Event: ' + id);
      alert('Received Event: ' + id)
  }
}
