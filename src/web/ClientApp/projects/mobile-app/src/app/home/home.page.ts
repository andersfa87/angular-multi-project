import { Component } from '@angular/core';
import { Camera } from '@ionic-native/camera';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: [],
})
export class HomePage {

    constructor(private camera: Camera) {}

    showCamera() {
        this.camera.getPicture();
      }
}