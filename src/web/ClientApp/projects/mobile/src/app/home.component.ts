import { Component } from '@angular/core';
import { BaseHomeComponent } from 'src/app/home.component';
import { TrackingService } from 'src/services/trackingService';

@Component({
  selector: 'home-page',
  templateUrl: './home.component.html',
  styleUrls: []
})
export class HomeComponent extends BaseHomeComponent {
  constructor(trackingService: TrackingService) { 
    super(trackingService)
  }
}
