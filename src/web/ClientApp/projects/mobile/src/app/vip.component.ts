import { Component, OnInit } from '@angular/core';
import { BaseVipComponent } from 'src/app/vip.component';
import { TrackingService } from 'src/services/trackingService';

@Component({
    selector: 'af-vip-page',
    templateUrl: './vip-page.component.html',
    styleUrls: []
})
export class VipComponent extends BaseVipComponent {
    constructor(trackingService: TrackingService) {
        super(trackingService);
    }

    
}
