import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { VipComponent } from './vip.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'vip/:id',
    component: VipComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
